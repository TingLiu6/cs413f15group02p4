package edu.luc.etl.cs313.android.simplestopwatch.test.android;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import android.content.pm.ActivityInfo;
import org.junit.Test;

import android.widget.Button;
import android.widget.TextView;
import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.android.SimpleTimerAdapter;

/**
 * Abstract GUI-level test superclass of several essential stopwatch scenarios.
 *
 * @author laufer
 *
 * TODO move this and the other tests to src/test once Android Studio supports
 * non-instrumentation unit tests properly.
 */
public abstract class AbstractSimpleTimerActivityTest {

    /**
     * Verifies that the activity under test can be launched.
     */
    public void testActivityCheckTestCaseSetUpProperly() {
        assertNotNull("Activity should be launched successfully", getActivity());
    }

    /**
     * Verifies the following scenario: time is 0.
     *
     * @throws Throwable
     */
    @Test
    public void testActivityScenarioInit() throws Throwable {
        getActivity().runOnUiThread(() -> assertEquals(0, getDisplayedValue()));
    }

    /**
    * testActivityScenarioInc performs these checks:
    * There should be a button that is able to increase the number on the display,
    * and it should be increased the value by 1.
     *
     * @throws Throwable
     */
    @Test
    public void testActivityScenarioInc() throws Throwable {
        getActivity().runOnUiThread(() -> {
            assertEquals(0, getDisplayedValue());
            assertTrue(getButton().performClick());
        });
        runUiThreadTasks();
        getActivity().runOnUiThread(() -> {
            assertEquals(1, getDisplayedValue());
        });
    }


    // auxiliary methods for easy access to UI widgets

    protected abstract SimpleTimerAdapter getActivity();

    protected int tvToInt(final TextView t) {
        return Integer.parseInt(t.getText().toString().trim());
    }

    protected int getDisplayedValue() {
        final TextView ts = (TextView) getActivity().findViewById(R.id.seconds);
        return tvToInt(ts);
    }

    protected Button getButton() {
        return (Button) getActivity().findViewById(R.id.button);
    }

    protected String getState() {
        final TextView ts = (TextView) getActivity().findViewById(R.id.stateName);
        return ts.getText().toString();
    }

    protected String getButtonText() {
        final TextView bt = (TextView)getActivity().findViewById(R.id.button);
        return bt.getText().toString();
    }

    /**
     * Explicitly runs tasks scheduled to run on the UI thread in case this is required
     * by the testing framework, e.g., Robolectric.
     */
    protected void runUiThreadTasks() { }
}
