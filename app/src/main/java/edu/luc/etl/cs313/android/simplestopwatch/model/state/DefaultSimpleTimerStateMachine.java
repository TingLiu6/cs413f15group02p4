package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import java.util.Timer;
import java.util.TimerTask;

import edu.luc.etl.cs313.android.simplestopwatch.common.SimpleTimerUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.counter.BoundedCounterModel;

/**
 * An implementation of the state machine for the simpletimer.
 *
 * @author laufer
 */
public class DefaultSimpleTimerStateMachine implements SimpleTimerStateMachine {

    public DefaultSimpleTimerStateMachine(final ClockModel clockModel, final BoundedCounterModel boundedcounterModel) {
        this.clockModel = clockModel;
        this.boundedcounterModel = boundedcounterModel;           //added on 4/42016
    }

    private final ClockModel clockModel;

    private final BoundedCounterModel boundedcounterModel;        //added on 4/42016

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private SimpleTimerState state;

    protected void setState(final SimpleTimerState state) {
        this.state = state;
        uiUpdateListener.updateState(state.getId());
    }

    private SimpleTimerUIUpdateListener uiUpdateListener;

    @Override
    public void setUIUpdateListener(final SimpleTimerUIUpdateListener uiUpdateListener) {
        this.uiUpdateListener = uiUpdateListener;
    }

    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread
    @Override public synchronized void onClickButton() { state.onClickButton(); }
    @Override public synchronized void onTick()        { state.onTick(); }

    @Override public void updateUIRuntime()   { uiUpdateListener.updateTime(boundedcounterModel.getRuntime()); }
    @Override public void updateButtonName()  { uiUpdateListener.updateButton(state.getId());}        //added on 4/1/2016
    @Override public void updateCountValue()  { uiUpdateListener.updateCount(); }                     //added on 4/4/2016
    @Override public void actionBeep()        { uiUpdateListener.playDefaultALARM(); }                //added on 4/10/2016

    // known states
    private final SimpleTimerState STOPPED     = new StoppedState(this);
    private final SimpleTimerState RUNNING     = new RunningState(this);
    private final SimpleTimerState ALARM       = new AlarmState(this);

    // transitions
    @Override public void toRunningState()    { setState(RUNNING); }
    @Override public void toStoppedState()    { setState(STOPPED); }
    @Override public void toAlarmState()      { setState(ALARM); }

    // actions
    @Override public void actionInit()       { toStoppedState(); actionReset(); }
    @Override public void actionReset()      { boundedcounterModel.resetRuntime(); actionUpdateView(); }
    @Override public void actionCancel()      { boundedcounterModel.reset(); updateCountValue(); }
    @Override public void actionStart()      { clockModel.start(); actionUpdateView();}  //added on 4/4/2016
    @Override public void actionStop()       { clockModel.stop(); }

    /**
     * plays one sound when meets condition
     */
    @Override public void actionAlarm()      { startAlarm();}  //added on 4/10/2016
    /**
     * Cancel the alarmtimer and set the runtime to 00
     */
    @Override public void actionStopAlarm()   {stopAlarm();actionReset();}   //added on 4/10/2016

    @Override public void actionIncrement()   { boundedcounterModel.increment(); actionUpdateView(); }
    @Override public void actionDecrement()   { boundedcounterModel.decRuntime(); actionUpdateView();}

    /**
     * Returns the remain time on the display which is decremented 1 for every second
     * The original time is the click count after click the "Increment" button for time(s)
     * @return the remain time on the display
     */
    @Override public int getClickcount()      {return boundedcounterModel.getRuntime();}   //added on 4/4/2016 time remain = getClickcount()
    @Override public void actionUpdateView()   { state.updateView(); }
    /**
     * Returns the click count after click the "Increment" button for time(s)
     * @return the click count
     */
    @Override public int getValue()            { return boundedcounterModel.getClickValue();}        //added on 4/4/2016

    public void onAlarm()       { actionBeep(); }

    private Timer alarmtimer;

    /**
     * Use a timer to play a continually and infinitely arlarm sound when meets a specific condition
     */
    public void startAlarm() {
        alarmtimer = new Timer();
        //added on 4/9/2016
        // The clock model runs onAlarm every 2000 milliseconds
        alarmtimer.schedule(new TimerTask() {
            @Override public void run() {
                // fire event
                onAlarm();
            }
        }, /*initial delay*/ 0, /*periodic delay*/ 2000);
    }

    /**
     * Cancel the alarm timer
     */
    public void stopAlarm() {
        alarmtimer.cancel();
    }
}
