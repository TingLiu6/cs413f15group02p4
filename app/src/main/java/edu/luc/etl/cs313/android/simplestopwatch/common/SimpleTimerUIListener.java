package edu.luc.etl.cs313.android.simplestopwatch.common;

/**
 * A listener for simpletinmer events coming from the UI.
 *
 * @author laufer
 */
public interface SimpleTimerUIListener {
    void onClickButton();
    int getValue();

}
