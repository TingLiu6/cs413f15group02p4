package edu.luc.etl.cs313.android.simplestopwatch.test.model.time;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.luc.etl.cs313.android.simplestopwatch.model.counter.BoundedCounterModel;

/**
 * Testcase superclass for the time model abstraction.
 * This is a simple unit test of an object without dependencies.
 *
 * @author laufer
 * @see http://xunitpatterns.com/Testcase%20Superclass.html
 */
public abstract class AbstractBoundedCounterModelTest {

    private BoundedCounterModel model;

    /**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     * @param model
     */
    protected void setModel(final BoundedCounterModel model) {
        this.model = model;
    }

    /**
     * testInitiallyAtMin:the timer is initially at 0
      */

    @Test
    public void testPreconditions() {
        assertEquals(0, model.getRuntime());
    }

    /**
     * testIncrement:one is added to the displayed value
     */

    @Test
    public void testIncrementtimeOne() {
        final int rt = model.getClickValue();
        model.increment();
        assertEquals((rt + 1), model.getClickValue());
    }

    /**
     * testFullAtMax:the maximum number that can be displayed on the timer is 99
     */
    @Test
    public void tesFullAtMax() {
        final int rt = model.getClickValue();
        for (int i = 0; i < 99; i ++) {
            model.increment();
        }
        assertEquals(99, model.getClickValue());
    }

     /**
     *testGet:the timer value is consistent across gets
     * testIsFull:the timer isn’t full if decremented at full (99)
     */
    @Test
    public void tesIsFull() {
        final int rt = model.getRuntime();
        for (int i = 0; i < 101; i ++) {
            model.increment();
        }
        assertTrue(model.isFull());

    }

    /**
     * testIsEmpty:the timer isn’t empty if incremented at empty (0)
     */
    @Test
    public void tesIsEmpty() {
        final int rt = model.getRuntime();
        for (int i = 10; i >0; i --) {
            model.decrement();
        }
        assertTrue(model.isEmpty());
    }

}
