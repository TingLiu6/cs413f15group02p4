package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.os.CountDownTimer;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class StoppedState implements SimpleTimerState {

    public StoppedState(final SimpleTimerSMStateView sm) {
        this.sm = sm;
    }

    private final SimpleTimerSMStateView sm;

    CountDownTimer cdt;

    /**
     * This methods plays the incrementing role of the project
     * clickcount = getValue,
     * if 0 < clickcount < 99, wait for 3 second, then count down
     * if clickcount = 99, count down immediately
   */
    @Override
    public void onClickButton() {
        if(cdt!= null)
            cdt.cancel();
        sm.actionIncrement();
        sm.actionUpdateView();

        if (this.getValue() > 0 && this.getValue() < 99) {

            cdt = new CountDownTimer(3000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    sm.actionBeep();
                    sm.actionStart();
                    sm.toRunningState();
                    sm.updateButtonName();
                }
            }.start();
        }


        else if (this.getValue() == 99)
        {
            sm.actionBeep();
            sm.actionStart();
            sm.toRunningState();
            sm.updateButtonName();
        }

        else sm.toStoppedState();
    }
    @Override
    public void onTick() {
        throw new UnsupportedOperationException("onTick");
    }

    @Override
    public int getId() {
        return R.string.STOPPED;
    }

    @Override
    public int getValue()
    {
        return sm.getValue();
    }

    @Override
    public void updateView() {
            sm.updateCountValue();
    }

}
